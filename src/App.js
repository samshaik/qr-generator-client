import React, { useState } from "react";
import "./App.css";
import { generateQRCode } from "./qr"; // Import the function

function App() {
  const [url, setUrl] = useState("");
  const [qrCodeSrc, setQRCodeSrc] = useState("");

  const handleGenerateQRCode = async () => {
    try {
      const qrCodeSrc = await generateQRCode(url);
      setQRCodeSrc(qrCodeSrc);
    } catch (error) {
      throw error;
    }
  };

  return (
    <div className="App">
      <h1>QR Code Generator</h1>
      <input
        type="text"
        placeholder="Enter URL"
        value={url}
        onChange={(e) => setUrl(e.target.value)}
      />
      
      <button onClick={handleGenerateQRCode}>Generate QR Code</button>
      {qrCodeSrc && <img src={qrCodeSrc} alt="QR Code" />}
    </div>
  );
}

export default App;
