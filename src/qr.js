import axios from "axios";
export const generateQRCode = async (url) => {
  try {
    const response = await axios.get(
      `${process.env.REACT_APP_BASE_URL}/generate?url=${encodeURIComponent(url)}`
    );
    console.log(response)
    if(response.status === 200){
        return `data:image/png;base64,${response.data.imageBase64}`;
    }else{
        return 'Something went wrong'
    }
  } catch (error) {
    console.error("Error generating QR code:", error);
    throw error;
  }
};
